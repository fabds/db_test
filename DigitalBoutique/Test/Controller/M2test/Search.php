<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Controller\M2test;

use DigitalBoutique\Test\Api\LogsRepositoryInterface;
use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use DigitalBoutique\Test\Api\Data\LogsInterfaceFactory;
use Magento\Framework\Phrase;

class Search extends Action implements HttpGetActionInterface
{

    /**
     * CONST
     */
    const PRODUCT_SKU = 'product_sku';
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var LogsInterfaceFactory
     */
    private $logsFactory;
    /**
     * @var LogsRepositoryInterface
     */
    private $logsRepository;
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @param Context $context
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param LogsInterfaceFactory $logsFactory
     * @param LogsRepositoryInterface $logsRepository
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        LogsInterfaceFactory $logsFactory,
        LogsRepositoryInterface $logsRepository,
        Session $customerSession
    )
    {
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->logsFactory = $logsFactory;
        $this->logsRepository = $logsRepository;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $productSku = "";
        if($this->request->has(self::PRODUCT_SKU)){
            $productSku = $this->request->getParam(self::PRODUCT_SKU);
        }
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if(empty($productSku)){
            $this->messageManager->addWarningMessage("Please insert a sku");
            return $redirect->setUrl($this->_redirect->getRefererUrl());
        }

        try {
            $log = $this->logsFactory->create()
                ->setSku($productSku)
                ->setCustomerType(($this->customerSession->isLoggedIn())?"customer":"guest")
                ->setCustomerId($this->customerSession->getId() ?? null);
            $this->logsRepository->save($log);
            $product = $this->productRepository->get($productSku);
            if($product->getStatus()==Status::STATUS_DISABLED) {
                throw new Exception('The product requested is not enabled');
            }
            if($product->getVisibility()==Visibility::VISIBILITY_NOT_VISIBLE) {
                throw new Exception('The product is not visible individually');
            }
            return $redirect->setUrl($product->getProductUrl());
        }
        catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $redirect->setUrl($this->_redirect->getRefererUrl());
    }

}

