<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Controller\Adminhtml\Logs;

use DigitalBoutique\Test\Api\Data\LogsInterfaceFactory;
use DigitalBoutique\Test\Api\LogsRepositoryInterface;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;

class Save extends Action implements HttpPostActionInterface
{
    /**
     * @var LogsInterfaceFactory
     */
    private $logsFactory;

    /**
     * @var LogsRepositoryInterface
     */
    private $logsRepository;

    /**
     * @param Context $context
     * @param LogsRepositoryInterface $logsRepository
     * @param LogsInterfaceFactory $logsFactory
     */
    public function __construct(
        Action\Context $context,
        LogsRepositoryInterface $logsRepository,
        LogsInterfaceFactory $logsFactory
    )
    {
        parent::__construct($context);
        $this->logsRepository = $logsRepository;
        $this->logsFactory = $logsFactory;
    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $id = $this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();
        unset($params['id']);
        if($params['customer_id']==""){
            $params['customer_id'] = null;
        }

        try{
            if(!empty($id)){
                $log = $this->logsRepository->get($id);
            }
            else {
                $log = $this->logsFactory->create();
            }
            $log->setAllData($params);
            $this->logsRepository->save($log);
            $this->messageManager->addSuccessMessage(__('Your log has been saved!'));
        }
        catch (Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error while trying to save log'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/edit/id/' . $id, ['_current' => true]);
    }
}
