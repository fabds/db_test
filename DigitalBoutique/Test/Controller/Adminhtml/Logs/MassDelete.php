<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Controller\Adminhtml\Logs;

use DigitalBoutique\Test\Api\LogsRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;

class MassDelete extends Action implements HttpPostActionInterface
{

    /**
     * @var LogsRepositoryInterface
     */
    private $logsRepository;

    /**
     * @param Action\Context $context
     * @param LogsRepositoryInterface $logsRepository
     */
    public function __construct(
        Action\Context $context,
        LogsRepositoryInterface $logsRepository
    )
    {
        parent::__construct($context);
        $this->logsRepository = $logsRepository;
    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $ids = $this->getRequest()->getParam('selected');

        try {
            foreach ($ids as $id) {
                $this->logsRepository->deleteById($id);
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', count($ids)));
        }
        catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error while trying to delete logs: ' . $e->getMessage()));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
    }
}
