<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Controller\Adminhtml\Logs;

use DigitalBoutique\Test\Api\LogsRepositoryInterface;
use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\Redirect;

class Delete extends Action implements HttpGetActionInterface
{

    /**
     * @var LogsRepositoryInterface
     */
    private $logsRepository;

    /**
     * @param Action\Context $context
     * @param LogsRepositoryInterface $logsRepository
     */
    public function __construct(
        Action\Context $context,
        LogsRepositoryInterface $logsRepository
    )
    {
        parent::__construct($context);
        $this->logsRepository = $logsRepository;
    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $id = $this->getRequest()->getParam('id');

        try {
            $this->logsRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('Your log has been deleted !'));
        }
        catch (Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error while trying to delete log'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
    }
}
