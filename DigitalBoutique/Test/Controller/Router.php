<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{

    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory
    )
    {
        $this->actionFactory = $actionFactory;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $result = null;

        if ($this->validateRoute($request)) {
            $request->setModuleName('digital-boutique')
                ->setControllerName('m2test')
                ->setActionName('index');
            $result = $this->actionFactory->create(Forward::class, ['request' => $request]);
        }

        return $result;
    }

    /**
     * @param RequestInterface $request
     * @return bool
     */
    public function validateRoute(RequestInterface $request): bool
    {
        $identifier = trim($request->getPathInfo(), '/');
        return strpos($identifier, 'digital-boutique/m2-test') !== false;
    }
}

