<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CustomerType implements OptionSourceInterface
{
    /**
     * Constants
     */
    const CUSTOMER = "customer";
    const GUEST = "guest";

    /**
     * @return array|array[]
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => self::CUSTOMER, 'label' => __('Customer')],
            ['value' => self::GUEST, 'label' => __('Guest')]
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::CUSTOMER => __('Customer'),
            self::GUEST => __('Guest')
        ];
    }
}
