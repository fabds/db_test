<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Model;

use DigitalBoutique\Test\Api\Data\LogsInterface;
use DigitalBoutique\Test\Api\Data\LogsInterfaceFactory;
use DigitalBoutique\Test\Api\LogsRepositoryInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ResourceConnectionFactory;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Zend_Db;

class LogsRepository implements LogsRepositoryInterface
{
    /**
     * @var LogsInterfaceFactory
     */
    private $logsFactory;
    /**
     * @var ResourceConnectionFactory
     */
    private $resourceConnectionFactory;
    /**
     * @var ResourceConnection
     */
    private $connection;

    /**
     * @param ResourceConnectionFactory $resourceConnectionFactory
     * @param LogsInterfaceFactory $logsFactory
     */
    public function __construct(
        ResourceConnectionFactory $resourceConnectionFactory,
        LogsInterfaceFactory $logsFactory
    ) {
        $this->logsFactory = $logsFactory;
        $this->resourceConnectionFactory = $resourceConnectionFactory;
    }

    /**
     * @return AdapterInterface
     */
    protected function getConnection()
    {
        if ($this->connection === null) {
            $this->connection = $this->resourceConnectionFactory->create()->getConnection();
        }

        return $this->connection;
    }

    /**
     * @param LogsInterface $logs
     * @return void
     */
    public function save(LogsInterface $logs)
    {
        $connection = $this->getConnection();
        $table = $connection->getTableName(LogsInterface::DB_TABLE);
        $connection->insertOnDuplicate(
            $table,
            [
                LogsInterface::ID => $logs->getId(),
                LogsInterface::SKU => $logs->getSku(),
                LogsInterface::CUSTOMER_TYPE => $logs->getCustomerType(),
                LogsInterface::CUSTOMER_ID => $logs->getCustomerId(),
            ],
            [
                LogsInterface::SKU,
                LogsInterface::CUSTOMER_TYPE,
                LogsInterface::CUSTOMER_ID
            ]
        );
    }

    /**
     * @param $id
     * @return LogsInterface
     * @throws NoSuchEntityException
     */
    public function get($id): LogsInterface
    {
        $connection = $this->getConnection();
        $table = $connection->getTableName(LogsInterface::DB_TABLE);
        $sql = $connection->select()->from($table, ['*'])->where('id = (?)', $id)->assemble();
        $res = $connection->fetchRow($sql, [], Zend_Db::FETCH_ASSOC);
        if ($res===null) {
            throw new NoSuchEntityException(new Phrase('The log with id ' . $id . ' doesn\'t exists'));
        }
        $logs = $this->logsFactory->create();

        return $logs->setAllData($res);
    }

    /**
     * @param LogsInterface $logs
     * @return void
     */
    public function delete(LogsInterface $logs)
    {
        $this->deleteById($logs->getId());
    }

    /**
     * @param $id
     * @return void
     */
    public function deleteById($id)
    {
        $connection = $this->getConnection();
        $table = $connection->getTableName(LogsInterface::DB_TABLE);
        $connection->delete($table,"id = {$id}");
    }
}
