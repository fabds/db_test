<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Model;

use DigitalBoutique\Test\Api\Data\LogsInterface;
use Magento\Framework\Model\AbstractModel;

class Logs extends AbstractModel implements LogsInterface
{

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(ResourceModel\Logs::class);
    }

    /**
     * @return array|mixed|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id): Logs
    {
        $this->setData(self::ID, $id);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getSku()
    {
        return $this->getData(self::SKU);
    }

    /**
     * @param $sku
     * @return $this
     */
    public function setSku($sku): Logs
    {
        $this->setData(self::SKU, $sku);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getCustomerType()
    {
        return $this->getData(self::CUSTOMER_TYPE);
    }

    /**
     * @param $customerType
     * @return $this
     */
    public function setCustomerType($customerType): Logs
    {
        $this->setData(self::CUSTOMER_TYPE, $customerType);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @param $customerId
     * @return $this
     */
    public function setCustomerId($customerId): Logs
    {
        $this->setData(self::CUSTOMER_ID, $customerId);
        return $this;
    }

    /**
     * @return array
     */
    public function getAllData(): array
    {
        $data = [];
        foreach (self::ALLOWED_ATTRIBUTES as $attribute) {
            $data[$attribute] = $this->getData($attribute);
        }
        return $data;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setAllData($data): Logs
    {
        foreach ($data as $key => $val) {
            if(in_array($key,self::ALLOWED_ATTRIBUTES)){
                $this->setData($key,$val);
            }
        }
        return $this;
    }


}