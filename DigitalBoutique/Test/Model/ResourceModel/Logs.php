<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Model\ResourceModel;

use DigitalBoutique\Test\Api\Data\LogsInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Logs extends AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(LogsInterface::DB_TABLE, LogsInterface::PRIMARY_KEY);
    }

}
