<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Model\ResourceModel\Logs;

use DigitalBoutique\Test\Api\Data\LogsInterface;
use DigitalBoutique\Test\Model\Logs;
use DigitalBoutique\Test\Model\ResourceModel\Logs as LogsResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = LogsInterface::PRIMARY_KEY;

    /**
     * Collection Constructor
     */
    protected function _construct()
    {
        $this->_init(Logs::class, LogsResource::class);
    }
}
