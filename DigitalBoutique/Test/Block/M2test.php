<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class M2test extends Template
{

    /**
     * Constructor
     *
     * @param Context  $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getFormAction(): string
    {
        return $this->getUrl('digital-boutique/m2test/search', ['_secure' => true]);
    }
}

