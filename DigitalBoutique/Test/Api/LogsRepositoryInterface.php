<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Api;

use DigitalBoutique\Test\Api\Data\LogsInterface;

interface LogsRepositoryInterface
{
    /**
     * @param LogsInterface $logs
     * @return mixed
     */
    public function save(LogsInterface $logs);

    /**
     * @param $id
     * @return mixed
     */
    public function get($id);

    /**
     * @param LogsInterface $logs
     * @return mixed
     */
    public function delete(LogsInterface $logs);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

}
