<?php

declare(strict_types=1);

namespace DigitalBoutique\Test\Api\Data;

interface LogsInterface
{
    /**
     * CONST
     */
    const DB_TABLE = 'm2test_logs';
    const PRIMARY_KEY = 'id';
    const ID = 'id';
    const SKU = 'sku';
    const CUSTOMER_TYPE = 'customer_type';
    const CUSTOMER_ID = 'customer_id';
    const ALLOWED_ATTRIBUTES = [self::ID, self::SKU, self::CUSTOMER_TYPE, self::CUSTOMER_ID];

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getSku();

    /**
     * @param $sku
     * @return mixed
     */
    public function setSku($sku);

    /**
     * @return mixed
     */
    public function getCustomerType();

    /**
     * @param $customerType
     * @return mixed
     */
    public function setCustomerType($customerType);

    /**
     * @return mixed
     */
    public function getCustomerId();

    /**
     * @param $customerId
     * @return mixed
     */
    public function setCustomerId($customerId);

    /**
     * @return mixed
     */
    public function getAllData();

    /**
     * @param $data
     * @return mixed
     */
    public function setAllData($data);

}