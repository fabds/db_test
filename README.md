# Mage2 Module DigitalBoutique Test

    ``digitalboutique/module-test``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)


## Main Functionalities
 - `{magento_fe_url}/digital-boutique/m2-test` or `{magento_fe_url}/digital-boutique/m2test` to search products through skus. Every search will be logged in DB.
 - `{magento_be_url}/digitalboutique/logs/index` to manage all search logs. There is an entry in Magento admin menu (Digital Boutique -> Log List)
 - `{magento_be_url}/digitalboutique/logs/edit` to edit a log entry.

## Installation

 - git clone `https://gitlab.com/fabds/db_test.git` in `app/code`
 - `php bin/magento module:enable DigitalBoutique_Test`
 - `php bin/magento setup:upgrade`
 - `php bin/magento setup:di:compile`
 - `php bin/magento cache:clean`